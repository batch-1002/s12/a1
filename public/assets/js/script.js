function fizzBuzz(num) {
	if (num % 3 === 0 && num % 5 === 0) {
		return "FizzBuzz";
	} else if (num % 3 === 0) {
		return "Fizz";
	} else if (num % 5 === 0) {
		return "Buzz";
	} else {
		return "Pop";
	}
}

console.log(fizzBuzz(5));
console.log(fizzBuzz(30));
console.log(fizzBuzz(27));
console.log(fizzBuzz(17));

function charToRainbowColor(char) {
	let sanitizedChar = char.toLowerCase();

	switch(sanitizedChar) {
		case "r":
			return "Red";
			break;
		case "o":
			return "Orange";
			break;
		case "y":
			return "Yellow";
			break;
		case "g":
			return "Green";
			break;
		case "i":
			return "Indigo";
			break;
		case "b":
			return "Blue";
			break;
		case "v":
			return "Violet";
			break;
		default:
			return "No color";
	}
}

console.log(charToRainbowColor("r"));
console.log(charToRainbowColor("G"));
console.log(charToRainbowColor("x"));
console.log(charToRainbowColor("B"));
console.log(charToRainbowColor("i"));


function isLeapYear(year) {
	if (year % 4 === 0 && 
		(year % 100 !== 0 || (year % 100 === 0 && year % 400 === 0))) {
			return "Leap year";
	} else {
		return "Not a leap year";
	}
}

console.log(isLeapYear(1900));
console.log(isLeapYear(2000));
console.log(isLeapYear(2004));
console.log(isLeapYear(2021));

